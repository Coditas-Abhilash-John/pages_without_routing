import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
 
  constructor() {}
  username = '';
  password = '';

  @Output() onLogin = new EventEmitter();

  ngOnInit(): void {}

  login() {
    if ('Abhi' !== this.username) {
      alert('Invalid Username');
    } else if ('1234' !== this.password) {
      alert('Invalid Password');
    } else {
      this.onLogin.emit();
    }
  }
}
