import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  @Output() navigateToAboutPage = new EventEmitter();

  showAboutPage(){
    this.navigateToAboutPage.emit(true);
    console.log('dsff');
  }
}
