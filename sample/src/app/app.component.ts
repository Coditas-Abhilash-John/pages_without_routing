import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'sample';
  isLoggedIn = false;
  showAboutPage = false;
  logUser() {
    this.isLoggedIn = true;
  }
  displayAbout() {
    this.showAboutPage = true;
  }
  logoutUser() {
    this.isLoggedIn = false;
  }
}
